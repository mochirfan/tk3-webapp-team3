import Header from "./Header";
import Product from "../Product/Product";

function Dashboard() {
  return (
    <>
      <Header />
      <div className="container">
        <Product />
      </div>
    </>
  );
}

export default Dashboard;
