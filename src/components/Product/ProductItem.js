import "./Product.css";
import ProductConfirmDelete from "./ProductConfirmDelete";
import ProductModal from "./ProductModal";
import { useState } from "react";

function ProductItem(props) {
  const { id, name, description, purchasePrice, sellPrice, file } = props.data;
  const [showDelete, setShowDelete] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const showConfirmDeleteHandler = () => setShowDelete(true);
  const confirmDeleteHiddenHandler = () => setShowDelete(false);

  const editModalHandler = (e) => setShowModal(true);
  const closeModalHandler = (e) => setShowModal(false);

  const deleteHandler = () => {
    props.onDelete(id);
    setShowDelete(false);
  };
  
  const updateHandler = (value) => {
    props.onUpdate(value);
    setShowModal(false);
  };

  return (
    <>
      <tr>
        <td>{props.index + 1}</td>
        <td>
          <img src={URL.createObjectURL(file[0])} alt={name} style={{ width: "55px" }} />
        </td>
        <td>{name}</td>
        <td>{description}</td>
        <td>{purchasePrice}</td>
        <td>{sellPrice}</td>
        <td>
          <button type="button" onClick={editModalHandler} className="btn btn-primary">
            <i className="fas fa-edit"></i>
          </button>
          <button type="button" onClick={showConfirmDeleteHandler} className="btn btn-danger ms-2">
            <i className="fas fa-trash"></i>
          </button>
        </td>
      </tr>
      <ProductConfirmDelete show={showDelete} onHide={confirmDeleteHiddenHandler} onDelete={deleteHandler} />
      <ProductModal show={showModal} onHide={closeModalHandler} onUpdate={updateHandler}  data={props.data} />
    </>
  );
}

export default ProductItem;
