import Modal from "react-bootstrap/Modal";
import { useState, useEffect } from "react";
import { generateGuid } from "../../utils/utils";

function ProductAdd(props) {
  const [isNew, setIsNew] = useState(true);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [purchasePrice, setPurchasePrice] = useState("");
  const [sellPrice, setSellPrice] = useState("");
  const [file, setFile] = useState("");

  useEffect(() => {
    if (props.show) {
      if (props.data) {
        const { name, description, purchasePrice, sellPrice, file } = props.data;
        setName(name);
        setDescription(description);
        setPurchasePrice(purchasePrice);
        setSellPrice(sellPrice);
        setFile(file);
        setIsNew(false);
      }
    } else {
      resetForm();
    }
  }, [props.show, props.data]);

  const nameChangeHandler = (e) => setName(e.target.value);
  const descriptionChangeHandler = (e) => setDescription(e.target.value);
  const purchasePriceChangeHandler = (e) => setPurchasePrice(e.target.value);
  const sellPriceChangeHandler = (e) => setSellPrice(e.target.value);

  const fileChangeHandler = (e) => {
    const files = Array.from(e.target.files);
    setFile(files);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (isNew) {
      props.onSubmit({
        id: generateGuid(),
        name,
        description,
        purchasePrice,
        sellPrice,
        file,
      });
    } else {
      props.onUpdate({
        id: props.data.id,
        name,
        description,
        purchasePrice,
        sellPrice,
        file,
      });
    }
  };

  const resetForm = () => {
    setName("");
    setDescription("");
    setPurchasePrice("");
    setSellPrice("");
    setFile("");
  };

  return (
    <Modal show={props.show} onHide={props.onHide} size="lg">
      <Modal.Header closeButton>
        <Modal.Title>{ isNew ? 'Tambah' : 'Ubah' }</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form id="formQuestion" onSubmit={submitHandler}>
          <div className="modal-body px-5">
            <div className="mb-3">
              <label htmlFor="exampleFormControlTextarea1" className="form-label">
                Nama
              </label>
              <input type="text" className="form-control" required value={name} onChange={nameChangeHandler} />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlTextarea1" className="form-label">
                Deskripsi
              </label>
              <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" required value={description} onChange={descriptionChangeHandler}></textarea>
            </div>
            <div className="row mb-3">
              <div className="col-6">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">
                  Harga Beli
                </label>
                <input type="number" className="form-control" required value={purchasePrice} onChange={purchasePriceChangeHandler} />
              </div>
              <div className="col-6">
                <label htmlFor="exampleFormControlTextarea1" className="form-label">
                  Harga Jual
                </label>
                <input type="number" className="form-control" required value={sellPrice} onChange={sellPriceChangeHandler} />
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleFormControlTextarea1" className="form-label">
                Gambar
              </label>
              <input type="file" className="form-control" required={isNew} onChange={fileChangeHandler} />
            </div>
            {file ? <img src={URL.createObjectURL(file[0])} alt={name} style={{ width: "120px" }} /> : "Not Uploaded Yet"}
          </div>
          <div className="modal-footer">
            <button className="btn btn-primary" type="submit">
              Kirim
            </button>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  );
}

export default ProductAdd;
