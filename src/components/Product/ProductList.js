import Table from "react-bootstrap/Table";
import ProductItem from "./ProductItem";

function ProductList(props) {
  return (
    <>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th style={{ width: "50px" }}>#</th>
            <th style={{ width: "60px" }}></th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th style={{ width: "150px" }}>Harga Beli</th>
            <th style={{ width: "150px" }}>Harga Jual</th>
            <th style={{ width: "120px" }}></th>
          </tr>
        </thead>
        <tbody>
          {props.data.map((m, index) => (
            <ProductItem data={m} key={m.id} index={index} onDelete={props.onDelete} onUpdate={props.onUpdate} />
          ))}
        </tbody>
      </Table>
    </>
  );
}

export default ProductList;
