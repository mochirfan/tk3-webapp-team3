import ProductList from "./ProductList";
import ProductModal from "./ProductModal";
import { useState } from "react";

function Product() {
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false);

  const addClickHandler = (e) => setShowModal(true);
  const closeModalHandler = (e) => setShowModal(false);

  const submitHandler = (value) => {
    data.push({ ...value });
    setShowModal(false);
  };

  const deleteHandler = (id) => {
    setData((prev) => prev.filter((f) => f.id !== id));
  };

  const updateHandler = (value) => {
    console.log('a');
    setData((prev) => {
      const itemIndex = prev.findIndex((f) => f.id === value.id);
      return [...prev.slice(0, itemIndex), { ...value }, ...prev.slice(itemIndex + 1)];
    });
  };

  return (
    <div className="p-5">
      <div className="text-end">
        <button className="btn btn-primary ms-auto mb-3" type="button" onClick={addClickHandler}>
          Tambah
        </button>
      </div>
      <ProductList data={data} onDelete={deleteHandler} onUpdate={updateHandler} />
      <ProductModal show={showModal} onHide={closeModalHandler} onSubmit={submitHandler} />
    </div>
  );
}

export default Product;
