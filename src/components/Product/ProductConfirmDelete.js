import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

function ProductConfirmDelete(props) {
  return (
    <Modal show={props.show} onHide={props.onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Hapus Produk</Modal.Title>
      </Modal.Header>
      <Modal.Body>Apakah anda yakin akan menghapus produk ini?</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={props.onHide}>
          Cancel
        </Button>
        <Button variant="danger" onClick={props.onDelete}>
          Delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ProductConfirmDelete;
