import "./Login.css";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import Alert from "react-bootstrap/Alert";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showError, setShowError] = useState(false);
  const navigate = useNavigate();

  const usernameChangeHandler = (e) => {
    setUsername(e.target.value);
  };
  const passwordChangeHandler = (e) => {
    setPassword(e.target.value);
  };

  const setLogin = (data) => {
    localStorage.setItem("userInfo", JSON.stringify(data));
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (username === "admin" && password === "admin") {
      setShowError(false);
      setLogin({ username });
      navigate("/dashboard");
    } else {
      setShowError(true);
    }
  };

  return (
    <div className="login-form-container">
      <form className="login-form" onSubmit={submitHandler}>
        <div className="login-form-content">
          <h3 className="login-form-title">Login</h3>
          <div className="form-group mt-3">
            <label>Username</label>
            <input type="text" value={username} onChange={usernameChangeHandler} className="form-control mt-1" placeholder="Enter username" required />
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input type="password" value={password} onChange={passwordChangeHandler} className="form-control mt-1" placeholder="Enter password" required />
          </div>
          {showError ? (
            <Alert className="mt-3 text-center" variant="danger">
              Username atau password salah
            </Alert>
          ) : (
            <span></span>
          )}
          <div className="d-grid gap-2 mt-3">
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </div>
          <Alert className="mt-3 text-center" variant="info">
            username: <strong>admin</strong> dan password: <strong>admin</strong>
          </Alert>
        </div>
      </form>
    </div>
  );
}

export default Login;
